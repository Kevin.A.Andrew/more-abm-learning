//
//  main.cpp
//  deepabm
//
//  Created by Kevin Andrew on 6/14/18.
//  Copyright © 2018 Kevin Andrew. All rights reserved.
//

#include <iomanip>
using std::setw;
#include <iostream>
using std::cout;
using std::endl;
using std::flush;
#ifdef __WIN32
#warning getopt not supported on windows
#else
#include <getopt.h>
#endif

#include <memory>
using std::unique_ptr;
#include <string>
using std::string;
#include <cstring>
using std::strcmp;

#include "dbmexcept.h"
#include "simulation.h"

#define BF(X) "\033[1m" X "\033[0m"
#define BOLD  "\033[1m"
#define DONE  "\033[0m"

void print_help() {
  // Header Info
  printf(BF("Name:\n"));
  printf("\tdabm - farmer simulation with ddqn learning\n\n");
  printf(BOLD "Synopsis:\n" DONE);
  printf("\tdabm [options]\n\n");
  // Options Info
  printf(BOLD "Options:\n" DONE);
  printf("\t-?, -h, --help\n");
  printf("\t\tDisplay this help info and exit\n");
  printf("\t-v, --verbose\n");
  printf("\t\tPrint more output\n");
  printf("\t-o, --octave\n");
  printf("\t\tPrint output as matrix\n");
  printf("\t-b, --brief\n");
  printf("\t\tPrint less output\n");
  printf("\t-e, --episodes=<eps>\n");
  printf("\t\tSets the number of training episodes to <eps>\n");
  printf("\t\tfor each agent.\n");
  printf("\t-p, --policy=<pol: \"GREEDY\", \"NONGREEDY\", \"RANDOM\">\n");
  printf("\t\tSets the policy of all agents to <pol>.\n");
  printf("\t-j, --jobs=<jbs=7>\n");
  printf("\t\tSets the number of system threads to <jbs>, default is 7.\n");
  printf("\n");
  // End Info
  printf(BOLD "Copyright:\n" DONE);
  printf("\t\t2018\n\n");
  printf(BOLD "Author:\n" DONE);
  printf("\t\tKevin Andrew\n\n");
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv) {
    unique_ptr<string> mat_prefix = nullptr;
    unique_ptr<string> govnet = nullptr;
    unsigned training_episodes = 100;
#ifndef __WIN32
    static struct option long_options[] = {
        {"verbose", no_argument, &verbose_flag, 1},
        {"brief", no_argument, &verbose_flag, 0},
        {"octave", optional_argument, 0, 'o'},
        {"govnet", required_argument, 0, 'g'},
        {"policy", required_argument, 0, 'p'},
        {"episodes", required_argument, 0, 'e'},
        {"jobs", required_argument, 0, 'j'},
        {"help", no_argument, 0, 'h'},
        {0,0,0,0}
    };
    int option_index;
    const char* opt_string = "vj:o::be:g:p:h?";
    
    int option;
    while ((option = getopt_long(argc, argv, opt_string, long_options,
                                 &option_index)) != -1) {
        switch(option) {
            case 0:
                break;
            case 'e':
                training_episodes = atoi(optarg);
                break;
            case 'g':
                // TODO: Implement GovNet Specification
                if (!strcmp(optarg, "DUMMY"))
                    return 1;
                govnet = make_unique<string>(optarg);
                break;
            case 'p':
                // Homo-Policy Selection
                {
                  string greedy("GREEDY");
                  string nongreedy("NONGREEDY");
                  string random("RANDOM");
                  if (!greedy.compare(optarg))
                    policy_val = 1;
                  else if (!nongreedy.compare(optarg))
                    policy_val = 2;
                  else if (!random.compare(optarg))
                    policy_val = 3;
                  else {
                    print_help();
                    return 1;
                  }
                }
                break;
            case 'o':
                octave_flag = 1;
                if (optarg == NULL)
                  mat_prefix = make_unique<string>("DAT");
                else
                  mat_prefix = make_unique<string>(optarg);
                break;
            case 'j':
                try {
                  job_count = atoi(optarg);
                  if (job_count <= 1)
                    throw 999;
                } catch (...) {
                  print_help();
                  return 1;
                }
                break;
            case 'b':
                verbose_flag = 0;
                break;
            case 'v':
                verbose_flag = 1;
                break;
            case 'h':
                [[fallthrough]];
            case '?':
                print_help();
                return 0;
            default:
                printf("Invalid Option: Try `dabm -?'\n");
                return 1;
        }
    }
    if (verbose_flag and octave_flag) {
      printf("Incompatible Options: --verbose and --octave");
      return 1;
    }
    if (!job_count)
      job_count = 7;
    if (!policy_val)
      policy_val = -1;
#endif
    Simulation model;
    
    if (verbose_flag)
        printf("Training\n");
    if (octave_flag)
        cout << *mat_prefix << "=[";
    for (unsigned episode = 1; episode <= training_episodes; episode++) {
        if (verbose_flag)
          cout << "EP" << setw(4) << episode << "\t";

        // Main Execution Body
        model.run_episode(40);

        if (verbose_flag)
          cout << endl;
    }
    if (octave_flag)
      cout << "];" << endl;
    return 0;
}

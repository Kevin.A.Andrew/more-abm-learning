//
//  environment.h
//  deepabm
//
//  Created by Kevin Andrew on 6/21/18.
//  Copyright © 2018 Kevin Andrew. All rights reserved.
//

#pragma once
#include <memory>
#include <vector>

class Environment {
    unsigned storm_count;
// TODO std::unique_ptr<std::vector<unsigned>> storms;
public:
    Environment(unsigned num_storms=0u);
    [[nodiscard]] inline unsigned storm_data() { return storm_count; }
};

Environment::Environment(unsigned num_storms): storm_count(num_storms) {}


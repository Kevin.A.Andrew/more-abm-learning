#pragma once

#include <memory>
using std::make_shared;
using std::make_unique;
using std::shared_ptr;
using std::unique_ptr;
#include <numeric>
using std::accumulate;
#include <stdexcept>
#include <utility>
using std::pair;
#include <vector>
using std::vector;

#include "environment.h"
#include "parcel.h"

class Landowner : public agent {
	unsigned landowner_id;
	unique_ptr<vector<shared_ptr<Parcel>>> owned_parcels;

protected:
    struct State {
        Landowner* land_owner;
        vector<long double> land_areas;
        State(Landowner* owner = nullptr) : land_owner(owner) {
            land_areas = *(new vector<long double>(0));
        }
        virtual void init() {
            land_areas = land_owner->owned_parcel_areas();
        }
        virtual operator vector<long double>() = 0;
        [[nodiscard]] virtual long double get() noexcept = 0;
    };
    
    struct Action {
        virtual long double operator[] (unsigned) = 0;
        virtual operator vector<long double>() = 0;
    };
    
    struct Record {
        State* state;
        Action* action;
        long double reward;
        State* next_state;
        Record() {}
        Record(State* state_t, Action* action_t):
            state(state_t), action(action_t) {}
    };
    
public:
	typedef agent super;

	Landowner(unsigned);
	Landowner(unsigned, shared_ptr<Parcel>);

	[[nodiscard]] inline unsigned owned_parcel_count();
	[[deprecated("No Use")]] vector<LandownerType> owned_parcel_types();
	[[nodiscard]] long double owned_parcel_area(Parcel::LandType);
  [[nodiscard]] inline vector<long double> owned_parcel_areas();
	[[noreturn]] long double owned_parcel_revenue();
	[[noreturn]] long double owned_parcel_rent();

  virtual void act() {}
	virtual void act(std::shared_ptr<Environment>) = 0;
  [[deprecated("No Use")]] virtual void act(unsigned) {}
  virtual void reset() = 0;

};

Landowner::Landowner(unsigned id) : super(id) {
	landowner_id = id;
	owned_parcels = make_unique<vector<shared_ptr<Parcel>>>();
}

Landowner::Landowner(unsigned id, shared_ptr<Parcel> parcel) : super(id) {
	landowner_id = id;
	owned_parcels = make_unique<vector<shared_ptr<Parcel>>>();
	owned_parcels->push_back(parcel);
}

inline unsigned Landowner::owned_parcel_count() {
	return (unsigned) owned_parcels->size();
}

vector<LandownerType> Landowner::owned_parcel_types() {
	return *(new vector<LandownerType>());
}

long double Landowner::owned_parcel_area(Parcel::LandType land_type) {
	return accumulate(owned_parcels->begin(), owned_parcels->end(), 0.0L,
			[&land_type] (long double sum, shared_ptr<Parcel> parcel) {
				return sum + parcel->get_area(land_type);
			});
}

vector<long double> Landowner::owned_parcel_areas() {
    if (owned_parcels->size() > 1)
        throw std::logic_error("Not Yet Implemented");
    else if (owned_parcels->size() == 0)
        throw std::logic_error("No Parcels");
    return owned_parcels->at(0)->get_areas();
}

long double Landowner::owned_parcel_revenue() {
    throw std::logic_error("Not Yet Implemented");
}

long double Landowner::owned_parcel_rent() {
    throw std::logic_error("Not Yet Implemented");
}


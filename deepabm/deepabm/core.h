#pragma once

#include <memory>
using std::make_unique;
using std::unique_ptr;
#include <string>
using std::string;
#include <thread>
using std::thread;
#include <type_traits>
#include <vector>
using std::vector;

#include "agent.h"
#include "config.h"
#include "landowner.h"

/*
	Temporary Class until Proper Back-end can be Finished
*/
template <class AgentType=agent>
class core {
protected:
	unsigned model_time = 0;
	unique_ptr<vector<unique_ptr<AgentType>>> agents;

public:
	core();
	core(string) {} 

	virtual void tick() = 0;
};

/*
	
*/
template <class AgentType>
core<AgentType>::core() {
	agents = make_unique<vector<unique_ptr<AgentType>>>();
}


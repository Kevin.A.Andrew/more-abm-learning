#pragma once 
#include <bitset>
#include <fstream>
#include <random>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

static int job_count;
static int policy_val;
static int verbose_flag;
static int octave_flag;

namespace config {
    struct {
        long double x;
        long double y;
    } NORTHWEST_CORNER, SOUTHEAST_CORNER;
    long PARCEL_COUNT;
    long FARMER_COUNT;
    long URBAN_BUSINESS_COUNT;
    long URBAN_RESIDENCE_COUNT;
    const struct production {
        long double MIN;
        long double MAX;
        long double MEAN;
        production(long double min, long double max):
            MIN(min), MAX(max), MEAN((min+max)/2.0L) {}
    } CORN_PROD(3.362551, 4.259232), HAY_PROD(0.358672, 0.470757),
      MEAT_PROD(900.0, 1200.0), MILK_PROD(210.0, 240.0),
      CORN_PHOS(2.02e-4, 6.17e-4), COW_PHOS(3.366e-4, 7.853e-4),
      HAY_PHOS(3.37e-5, 1.12e-4);
    
    const long double LAND_SCALE = 1650600.0L;
    const long double COW_SCALE = 1800.0L;
}

const long BOVINE_ANIMAL_MIN = 2;
const long BOVINE_ANIMAL_MAX = 4;

const unique_ptr<std::random_device> RANDOM_DEVICE = 
    make_unique<std::random_device>();

long sim_year = 0;

[[deprecated("In Deprecated Code-Block")]] void deprecated() {}

[[using std: string, getline, stol, stold, stringstream, vector]]
void read_header(std::ifstream& csv) {
	vector<long double> coords;
	for (int i = 0; i < 4; i++) {
		vector<string> result;
		string line, cell;
        
		getline(csv, line);
		stringstream line_stream(line);
        
		while (getline(line_stream, cell, ','))
			if (!cell.empty())
				result.push_back(cell);
		coords.push_back(stold(result[1]));
	}
    config::NORTHWEST_CORNER.x = coords[0];
    config::NORTHWEST_CORNER.y = coords[1];
    config::SOUTHEAST_CORNER.x = coords[2];
    config::SOUTHEAST_CORNER.y = coords[3];
    
    vector<long> params;
	for (int i = 0; i < 4; i++) {
		vector<string> result;
		string line, cell;
        
		getline(csv, line);
		stringstream line_stream(line);
        
		while (getline(line_stream, cell, ','))
			if (!cell.empty())
				result.push_back(cell);
		params.push_back(stol(result[1]));
	}
    config::PARCEL_COUNT = params[0];
    config::FARMER_COUNT = params[1];
    config::URBAN_BUSINESS_COUNT = params[2];
    config::URBAN_RESIDENCE_COUNT = params[3];
}

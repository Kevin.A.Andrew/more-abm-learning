#pragma once

#include <iostream>
using std::ostream;
#include <string>
using std::string;

#include "dbmexcept.h"
using dabm::agent_exception;

class agent {

protected:
	unsigned id;
	unsigned action_time = 1;

  virtual string to_ostr() {
      return std::to_string(id);
  }  

public:
	agent(unsigned);
    virtual ~agent() = 0;

  virtual unsigned get_id() { return id; }
  virtual void act() = 0;
	virtual void act(unsigned) = 0;
  virtual void end_episode() {}

  friend ostream& operator<<(ostream& os, agent& agt) {
    return os << agt.to_ostr();
  }
};

agent::~agent() {}

agent::agent(unsigned v_id) {
	id = v_id;
}


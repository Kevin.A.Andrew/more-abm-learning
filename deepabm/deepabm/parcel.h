#pragma once

#include <array>
#include <chrono>
using std::chrono::high_resolution_clock;
#include <random>
using std::default_random_engine;
using std::uniform_real_distribution;
#include <vector>
using std::vector;
#include <string>
using std::to_string;
#include <stdexcept>
using std::logic_error;

#include "config.h"
#include "logging.h"

enum LandownerType {
	FARMER=1,
	UBIZ=21,
	URES=22,
	FOUR=31,
	FIVE=32,
	SIX=41,
    NIL=0
};

class Parcel {
	static shared_ptr<default_random_engine> random_engine;
	static shared_ptr<uniform_real_distribution<long double>>
			rent_generator;

	unsigned long id;
	unsigned parcel_id;

	bool occupied;
	long double rent;

	struct {
		long double total;
		long double water;
		long double urban;
		long double barren;
		long double grass;
		long double forest;
		long double pasture;
		long double crop;
		long double wetland;
        vector<long double> get_areas() {
            return {{total, water, urban, barren,
                grass, forest, pasture, crop, wetland}};
        }
	} land_area;

	struct {
		long double x;
		long double y;
	} center;

public:
	enum LandType {
		TOTAL,
		WATER,
		URBAN,
		BARREN,
		GRASS,
		FOREST,
		PASTURE,
		CROP,
		WETLAND
	};
    constexpr static std::array<LandType, 9> LandTypes
			= {{TOTAL, WATER, URBAN, BARREN, GRASS, FOREST,
			PASTURE, CROP, WETLAND}};

	static unsigned count;

	Parcel();
	Parcel(long, LandownerType, vector<long double>,
			long double, long double);

	LandownerType landowner_type;
	inline long double get_area(LandType land_type) {
		switch (land_type) {
			case TOTAL:
				return land_area.total;
			case WATER:
				return land_area.water;
			case URBAN:
				return land_area.urban;
			case BARREN:
				return land_area.barren;
			case GRASS:
				return land_area.grass;
			case FOREST:
				return land_area.forest;
			case PASTURE:
				return land_area.pasture;
			case CROP:
				return land_area.crop;
			case WETLAND:
				return land_area.wetland;
			default:
				throw logic_error("Bad Enum Value");
		}
	}
    
    inline vector<long double> get_areas() {
        return land_area.get_areas();
    }

	inline long double get_rent() {
		return rent;
	}
};

shared_ptr<default_random_engine> Parcel::random_engine(
        make_shared<default_random_engine>((*RANDOM_DEVICE)()));

shared_ptr<uniform_real_distribution<long double>> Parcel::rent_generator(
		make_shared<uniform_real_distribution<long double>>(0.2, 0.9));

unsigned Parcel::count = 0u;

Parcel::Parcel () {
	occupied = true;
	rent = 200 + 100 * (*rent_generator)(*random_engine);
	parcel_id = count++;

	id = 0L;
	landowner_type = FARMER;

	land_area.total = 0.0;
	land_area.water = 0.0;
	land_area.urban = 0.0;
	land_area.barren = 0.0;
	land_area.grass = 0.0;
	land_area.forest = 0.0;
	land_area.pasture = 0.0;
	land_area.crop = 0.0;
	land_area.wetland = 0.0;

	center.x = 0.0;
	center.y = 0.0;
}

Parcel::Parcel(long v_id, LandownerType lo_type, 
		vector<long double> land_areas, long double x=0.0,
		long double y=0.0) {
	occupied = true;
	rent = 200 + 100 * (*rent_generator)(*random_engine);
	parcel_id = count++;

	id = v_id;
	landowner_type = lo_type;

	land_area.total = land_areas[0];
	land_area.water = land_areas[1];
	land_area.urban = land_areas[2];
	land_area.barren = land_areas[3];
	land_area.grass = land_areas[4];
	land_area.forest = land_areas[5];
	land_area.pasture = land_areas[6];
	land_area.crop = land_areas[7];
	land_area.wetland = land_areas[8];

	center.x = x;
	center.y = y;
}


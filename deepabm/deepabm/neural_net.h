//
//  neural_net.h
//  deepabm
//
//  Created by Kevin Andrew on 6/28/18.
//  Copyright © 2018 Kevin Andrew. All rights reserved.
//

#pragma once
#include <cmath>
#include <algorithm>
#include <memory>
using std::unique_ptr;
using std::shared_ptr;
using std::make_unique;
using std::make_shared;
#include <random>
using std::default_random_engine;
#include <stdexcept>
#include <vector>
using std::vector;

#include "config.h"
#include "learning.h"
#include "logging.h"

class neural_net {
    static shared_ptr<default_random_engine> weight_generator;
    
    unique_ptr<vector<vector<double>>> input_weights;
    unique_ptr<vector<vector<vector<double>>>> inner_weights;
    unique_ptr<vector<vector<double>>> output_weights;
    
    unique_ptr<vector<double>> input_values;
    unique_ptr<vector<vector<double>>> inner_values;
    unique_ptr<vector<double>> output_values;
    
    learning_mode mode = TRAINING;
    learning_rate alpha;
    double clip = 1.0;
    
    unsigned input_count;
    unsigned inner_layers;
    unsigned inner_count;
    unsigned output_count;
    
    inline double relu(double x) {
        return std::max<double>(x, 0.0L);
    }
    
    inline double td_relu(double x) noexcept {
        return (x > 0.0) ? 1.0 : 0.0;
    }
    
    inline double clip_td(double x) noexcept {
        if (clip <= 0.0)
            return x;
        return (x > clip) ? clip : (x < -clip) ? -clip : x;
    }
    
public:
    neural_net(const neural_net&);
    neural_net(unsigned, unsigned, unsigned, unsigned, learning_rate);
    neural_net(neural_net, neural_net, transfer_rate);
    
    vector<long double> get_output();
    vector<double> get_softmax();
    unsigned get_onehot();
    
    inline long double get_out() {
        if (output_count > 1)
            throw std::domain_error("Undefined for Output Range");
        return output_values->at(0);
    }
    
    inline neural_net* feed(vector<long double>);
    inline neural_net* feed(vector<long double>, vector<long double>);
    inline neural_net* feed(long double);
    inline neural_net* feed_grad(vector<long double>);
    
    void feed_forward(vector<long double>);
    void feed_backward(double);
    void feed_gradient_backward(vector<long double>);
    void transfer_in(neural_net, transfer_rate);
};

shared_ptr<default_random_engine> neural_net::weight_generator(
                                                               make_shared<default_random_engine>(default_random_engine((*RANDOM_DEVICE)())));

#pragma mark - Constructors

neural_net::neural_net(const neural_net& copy) {
    input_count = copy.input_count;
    inner_layers = copy.inner_layers;
    inner_count = copy.inner_count;
    output_count = copy.output_count;
    alpha = copy.alpha;
    
    input_values = make_unique<vector<double>>(input_count, 0.0);
    inner_values = make_unique<vector<vector<double>>>(inner_layers,
                                                       vector<double>(inner_count, 0.0));
    output_values = make_unique<vector<double>>(output_count, 0.0);
    
    input_weights = make_unique<vector<vector<double>>>(input_count+1,
                                                        vector<double>(inner_count, 0.0));
    inner_weights =
    make_unique<vector<vector<vector<double>>>>(inner_layers - 1,
                                                vector<vector<double>>(inner_count+1,
                                                                       vector<double>(inner_count, 0.0)));
    output_weights = make_unique<vector<vector<double>>>(inner_count + 1,
                                                         vector<double>(output_count, 0.0));
    
    for (unsigned input = 0; input <= input_count; input++)
        for (unsigned next = 0; next < inner_count; next++)
            (*input_weights)[input][next] =
            (*copy.input_weights)[input][next];
    for (unsigned layer = 0; layer < inner_layers - 1; layer++)
        for (unsigned input = 0; input <= inner_count; input++)
            for (unsigned next = 0; next < inner_count; next++)
                (*inner_weights)[layer][input][next] =
                (*copy.inner_weights)[layer][input][next];
    for (unsigned input = 0; input <= inner_count; input++)
        for (unsigned next = 0; next < output_count; next++)
            (*output_weights)[input][next] =
            (*copy.output_weights)[input][next];
}

neural_net::neural_net(unsigned input_nodes, unsigned hidden_layers,
                       unsigned hidden_nodes, unsigned output_nodes,
                       learning_rate l_rate = 0.05L) {
    input_count = input_nodes;
    inner_layers = hidden_layers;
    inner_count = hidden_nodes;
    output_count = output_nodes;
    alpha = l_rate;
    
    input_values = make_unique<vector<double>>(input_nodes, 0.0);
    inner_values = make_unique<vector<vector<double>>>(inner_layers,
                                                       vector<double>(inner_count, 0.0));
    output_values = make_unique<vector<double>>(output_count, 0.0);
    
    input_weights = make_unique<vector<vector<double>>>(input_count+1,
                                                        vector<double>(inner_count, 0.0));
    inner_weights =
    make_unique<vector<vector<vector<double>>>>(inner_layers - 1,
                                                vector<vector<double>>(inner_count+1,
                                                                       vector<double>(inner_count, 0.0)));
    output_weights = make_unique<vector<vector<double>>>(inner_count + 1,
                                                         vector<double>(output_count, 0.0));
    
    std::normal_distribution<double> he_initializer(0, 1);
    for (unsigned input = 0; input <= input_count; input++)
        for (unsigned next = 0; next < inner_count; next++)
            (*input_weights)[input][next] =
            (he_initializer(*weight_generator) * 2 / inner_count);
    for (unsigned layer = 0; layer < inner_layers - 1; layer++)
        for (unsigned input = 0; input <= inner_count; input++)
            for (unsigned next = 0; next < inner_count; next++)
                (*inner_weights)[layer][input][next] =
                (he_initializer(*weight_generator) * 2 / inner_count);
    for (unsigned input = 0; input <= inner_count; input++)
        for (unsigned next = 0; next < output_count; next++)
            (*output_weights)[input][next] =
            (he_initializer(*weight_generator) * 2 / inner_count);
}

neural_net::neural_net(neural_net target, neural_net learner,
                       transfer_rate tau) {
    input_count = target.input_count;
    inner_layers = target.inner_layers;
    inner_count = target.inner_count;
    output_count = target.output_count;
    alpha = target.alpha;
    
    input_values = make_unique<vector<double>>(input_count, 0.0);
    inner_values = make_unique<vector<vector<double>>>(inner_layers,
                                                       vector<double>(inner_count, 0.0));
    output_values = make_unique<vector<double>>(output_count, 0.0);
    
    input_weights = make_unique<vector<vector<double>>>(input_count+1,
                                                        vector<double>(inner_count, 0.0));
    inner_weights =
    make_unique<vector<vector<vector<double>>>>(inner_layers - 1,
                                                vector<vector<double>>(inner_count+1,
                                                                       vector<double>(inner_count, 0.0)));
    output_weights = make_unique<vector<vector<double>>>(inner_count + 1,
                                                         vector<double>(output_count, 0.0));
    
    for (unsigned input = 0; input <= input_count; input++)
        for (unsigned next = 0; next < inner_count; next++)
            (*input_weights)[input][next] =
            (1.0L - tau) * (*target.input_weights)[input][next]
            + tau * (*learner.input_weights)[input][next];
    for (unsigned layer = 0; layer < inner_layers - 1; layer++)
        for (unsigned input = 0; input <= inner_count; input++)
            for (unsigned next = 0; next < inner_count; next++)
                (*inner_weights)[layer][input][next] =
                (1.0L - tau) * (*target.inner_weights)[layer][input][next]
                + tau * (*learner.inner_weights)[layer][input][next];
    for (unsigned input = 0; input <= inner_count; input++)
        for (unsigned next = 0; next < output_count; next++)
            (*output_weights)[input][next] =
            (1.0L - tau) * (*target.output_weights)[input][next]
            + tau * (*learner.output_weights)[input][next];
}

void neural_net::transfer_in(neural_net learner, transfer_rate tau) {
    for (unsigned input = 0; input <= input_count; input++)
        for (unsigned next = 0; next < inner_count; next++)
            (*input_weights)[input][next] =
            (1.0L - tau) * (*input_weights)[input][next]
            + tau * (*learner.input_weights)[input][next];
    for (unsigned layer = 0; layer < inner_layers - 1; layer++)
        for (unsigned input = 0; input <= inner_count; input++)
            for (unsigned next = 0; next < inner_count; next++)
                (*inner_weights)[layer][input][next] =
                (1.0L - tau) * (*inner_weights)[layer][input][next]
                + tau * (*learner.inner_weights)[layer][input][next];
    for (unsigned input = 0; input <= inner_count; input++)
        for (unsigned next = 0; next < output_count; next++)
            (*output_weights)[input][next] =
            (1.0L - tau) * (*output_weights)[input][next]
            + tau * (*learner.output_weights)[input][next];
}

#pragma mark - Outputs

vector<long double> neural_net::get_output() {
    vector<long double> blah(output_count);
    for (unsigned i = 0; i < output_count; i++)
        blah[i] = (long double)(*output_values)[i];
    return blah;
}

[[using std: distance, max_element]]
vector<double> neural_net::get_softmax() {
    vector<double> softmax_values(output_count);
    
    double maximum = (*output_values)[distance(output_values->begin(),
                                               max_element(output_values->begin(), output_values->end()))];
    for (unsigned output_node = 0; output_node < output_count; output_node++)
        softmax_values[output_node] =
        std::expm1((*output_values)[output_node] - maximum);
    
    double total = accumulate(softmax_values.begin(), softmax_values.end(),
                              0.0L);
    for (unsigned output_node = 0; output_node < output_count; output_node++)
        softmax_values[output_node] /= total;
    
    return softmax_values;
}

[[using std: distance, max_element]]
unsigned neural_net::get_onehot() {
    vector<double> softmax = get_softmax();
    auto max_el = max_element(softmax.begin(), softmax.end());
    return (unsigned) distance(softmax.begin(), max_el);
}

#pragma mark - Functionality

inline neural_net* neural_net::feed(vector<long double> in_values) {
    this->feed_forward(in_values);
    return this;
}

inline neural_net* neural_net::feed(vector<long double> first_input,
                                    vector<long double> second_input) {
    vector<long double> input(first_input);
    input.insert(input.end(), second_input.begin(), second_input.end());
    this->feed_forward(input);
    return this;
}

inline neural_net* neural_net::feed(long double loss) {
    this->feed_backward((double)loss);
    return this;
}

inline neural_net* neural_net::feed_grad(vector<long double> gradient) {
    this->feed_gradient_backward(gradient);
    return this;
}

void neural_net::feed_forward(vector<long double> in_values) {
    if (in_values.size() != input_values->size())
        throw learning_exception("Nerual Net Problem: Feeding " + to_string(in_values.size())
                                 + " of " + to_string(input_values->size()) + " Values");
    
    for (unsigned input = 0; input < input_count; input++)
        (*input_values)[input] = in_values[input];
    
    // Input Layer
    for (unsigned hidden_node = 0; hidden_node < inner_count; hidden_node++) {
        double new_value = 0.0L;
        for (unsigned input_node = 0; input_node < input_count; input_node++)
            new_value += (*input_values)[input_node] *
            (*input_weights)[input_node][hidden_node];
        new_value += (*input_weights)[input_count][hidden_node];
        (*inner_values)[0][hidden_node] = relu(new_value);
    }
    
    // Hidden Layers
    for (unsigned layer = 1; layer < inner_layers; layer++) {
        for (unsigned hidden_node = 0; hidden_node < inner_count; hidden_node++) {
            double new_value = 0.0L;
            for (unsigned input_node = 0; input_node < inner_count; input_node++)
                new_value += (*inner_values)[layer-1][input_node]
                * (*inner_weights)[layer-1][input_node][hidden_node];
            new_value += (*inner_weights)[layer-1][inner_count][hidden_node];
            (*inner_values)[layer][hidden_node] = relu(new_value);
        }
    }
    
    // Output Layer
    for (unsigned output_node = 0; output_node < output_count; output_node++) {
        double new_value = 0.0L;
        for (unsigned input_node = 0; input_node < inner_count; input_node++)
            new_value += (*inner_values)[inner_layers-1][input_node]
            * (*output_weights)[input_node][output_node];
        new_value += (*output_weights)[inner_count][output_node];
        (*output_values)[output_node] = new_value;
    }
}

void neural_net::feed_backward(double loss) {
    if (loss > 10) loss = 10.0;
    if (loss < -10) loss = -10.0;
    
    if (mode == TESTING) {
        warn("attempting to feed loss to network in <TESTING> mode");
        return;
    }
    
    unique_ptr<vector<double>> output_errors =
    make_unique<vector<double>>(*output_values);
    unique_ptr<vector<vector<double>>> inner_deltas =
    make_unique<vector<vector<double>>>(inner_layers,
                                        vector<double>(inner_count+1, 0.0L));
    unique_ptr<vector<double>> input_deltas =
    make_unique<vector<double>>(input_count+1, 0.0L);
    
    for (unsigned output_node = 0; output_node < output_count; output_node++)
        (*output_errors)[output_node] *= loss;
    
    // Get Deltas
    for (unsigned input_node = 0; input_node <= inner_count; input_node++)
        for (unsigned output_node = 0; output_node < output_count; output_node++)
            (*inner_deltas)[inner_layers-1][input_node] +=
            (*output_errors)[output_node]
            * (*output_weights)[input_node][output_node];
    
    for (int layer = inner_layers-2; layer >= 0; layer--)
        for (unsigned input_node = 0; input_node <= inner_count; input_node++)
            for (unsigned output_node = 0; output_node < inner_count; output_node++)
                (*inner_deltas)[layer][input_node] +=
                clip_td((*inner_deltas)[layer+1][output_node]
                        * (*inner_weights)[layer][input_node][output_node]
                        * td_relu((*inner_values)[layer][input_node]));
    
    for (unsigned input_node = 0; input_node <= input_count; input_node++)
        for (unsigned output_node = 0; output_node < inner_count; output_node++)
            (*input_deltas)[input_node] += clip_td((*inner_deltas)[0][output_node]
                                                   * (*input_weights)[input_node][output_node]);
    
    // Apply Deltas
    for (unsigned input_node = 0; input_node <= inner_count; input_node++)
        for (unsigned output_node = 0; output_node < output_count; output_node++)
            (*output_weights)[input_node][output_node] =
            ((*output_weights)[input_node][output_node]
             + alpha * (*inner_deltas)[inner_layers-1][input_node]
             * (*inner_values)[inner_layers-1][input_node]);
    
    for (int layer = inner_layers-2; layer >= 0; layer--)
        for (unsigned input_node = 0; input_node <= inner_count; input_node++)
            for (unsigned output_node = 0; output_node < inner_count; output_node++)
                (*inner_weights)[layer][input_node][output_node] =
                ((*inner_weights)[layer][input_node][output_node]
                 + alpha * (*inner_deltas)[layer][input_node]
                 * (*inner_values)[layer][input_node]);
    
    for (unsigned input_node = 0; input_node <= input_count; input_node++)
        for (unsigned output_node = 0; output_node < inner_count; output_node++)
            (*input_weights)[input_node][output_node] =
            ((*input_weights)[input_node][output_node]
             + alpha * (*input_deltas)[input_node]
             * (*input_values)[input_node]);
}

void neural_net::feed_gradient_backward(vector<long double> loss) {
    if (mode == TESTING) {
        warn("attempting to feed loss to network in <TESTING> mode");
        return;
    }
    
    unique_ptr<vector<double>> output_errors =
    make_unique<vector<double>>(*output_values);
    unique_ptr<vector<vector<double>>> inner_deltas =
    make_unique<vector<vector<double>>>(inner_layers,
                                        vector<double>(inner_count+1, 0.0L));
    unique_ptr<vector<double>> input_deltas =
    make_unique<vector<double>>(input_count+1, 0.0L);
    
    for (unsigned output_node = 0; output_node < output_count; output_node++)
        (*output_errors)[output_node] *= loss[output_node];
    
    // Get Deltas
    for (unsigned input_node = 0; input_node <= inner_count; input_node++)
        for (unsigned output_node = 0; output_node < output_count; output_node++)
            (*inner_deltas)[inner_layers-1][input_node] +=
            clip_td((*output_errors)[output_node]
                    * (*output_weights)[input_node][output_node]);
    
    for (int layer = inner_layers-2; layer >= 0; layer--)
        for (unsigned input_node = 0; input_node <= inner_count; input_node++)
            for (unsigned output_node = 0; output_node < inner_count; output_node++)
                (*inner_deltas)[layer][input_node] +=
                clip_td((*inner_deltas)[layer+1][output_node]
                        * (*inner_weights)[layer][input_node][output_node]
                        * td_relu((*inner_values)[layer][input_node]));
    
    for (unsigned input_node = 0; input_node <= input_count; input_node++)
        for (unsigned output_node = 0; output_node < inner_count; output_node++)
            (*input_deltas)[input_node] += clip_td((*inner_deltas)[0][output_node]
                                                   * (*input_weights)[input_node][output_node]);
    
    // Apply Deltas
    for (unsigned input_node = 0; input_node <= inner_count; input_node++)
        for (unsigned output_node = 0; output_node < output_count; output_node++)
            (*output_weights)[input_node][output_node] =
            ((*output_weights)[input_node][output_node]
             + alpha * (*inner_deltas)[inner_layers-1][input_node]
             * (*inner_values)[inner_layers-1][input_node]);
    
    for (int layer = inner_layers-2; layer >= 0; layer--)
        for (unsigned input_node = 0; input_node <= inner_count; input_node++)
            for (unsigned output_node = 0; output_node < inner_count; output_node++)
                (*inner_weights)[layer][input_node][output_node] =
                ((*inner_weights)[layer][input_node][output_node]
                 + alpha * (*inner_deltas)[layer][input_node]
                 * (*inner_values)[layer][input_node]);
    
    for (unsigned input_node = 0; input_node <= input_count; input_node++)
        for (unsigned output_node = 0; output_node < inner_count; output_node++)
            (*input_weights)[input_node][output_node] =
            ((*input_weights)[input_node][output_node]
             + alpha * (*input_deltas)[input_node]
             * (*input_values)[input_node]);
}


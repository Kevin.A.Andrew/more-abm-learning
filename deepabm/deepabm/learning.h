#pragma once

#include <exception>
#include <string>

typedef long double discount_rate;
typedef long double learning_rate;
typedef long double transfer_rate;

enum learning_mode {
    TRAINING,
    TESTING
};

class[[using std: string]] learning_exception: public std::exception {
    string info;
public:
    learning_exception(const string& message) : info(message) {}
    ~learning_exception() {}
    
    const string message() noexcept {
        return info;
    }
    
    virtual const char* what() const noexcept {
        return info.c_str();
    }
};

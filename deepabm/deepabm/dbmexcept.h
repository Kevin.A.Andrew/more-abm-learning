#pragma once

#include <exception>
#include <string>

namespace dabm {
  class dbm_exception;
  class agent_exception;
  class lrn_exception;
}

class dabm::dbm_exception: public std::exception {
protected:
  std::string info;
public:
  dbm_exception(const std::string& message=""): info(message) {}
  ~dbm_exception() {}

  [[nodiscard]] inline const std::string message() const noexcept {
    return info;
  }

  [[nodiscard]] virtual const char* what() const noexcept {
    return info.c_str();
  }
};

class dabm::agent_exception: public dbm_exception {
public:
  agent_exception(const std::string& message="") {
    info = message;
  }
  ~agent_exception() {}
};

class dabm::lrn_exception: public dbm_exception {
public:
  lrn_exception() {}
};


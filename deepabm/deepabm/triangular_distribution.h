#pragma once
#include <random>
using std::uniform_real_distribution;

template <class T>
class triangular_distribution: public uniform_real_distribution<T> {
	static uniform_real_distribution<T> rng;
	T c;

public:
	typedef uniform_real_distribution<T> super;
	triangular_distribution(T min=0.0, T max=1.0);
	triangular_distribution(T, T, T);

	template <class Generator>
	T operator()(Generator&);
    template <class Generator>
    T operator()(Generator&, const T&, const T&);
};

template <class T>
uniform_real_distribution<T> triangular_distribution<T>::rng
		= uniform_real_distribution(0.0L, 1.0L);

#pragma mark - Constructors

template <class T>
triangular_distribution<T>::triangular_distribution(T min, T max)
		: super(min, max), c((min + max)/2) {}

template <class T>
triangular_distribution<T>::triangular_distribution(T min, T max, T mode)
		: super(min, max), c(mode) {}

#pragma mark - Operators

template <class T>
template <class Generator>
T triangular_distribution<T>::operator()(Generator& g) {
	T a = super::a(), b = super::b(); 
	T f_val = (c - a) / (b - a);
	T rand_val = rng(g);
	return (rand_val < f_val)
			? a + sqrt(rand_val * (b-a) * (c-a)) 
			: b - sqrt((1-rand_val) * (b-a) * (b - c));
}

template <class T>
template <class Generator>
/**
 Generates a number on a triangle distribution centered about mean with width range.

 @param g A random number generator.
 @param range The width of a target distribution.
 @param mean The mean of a target distribution.
 @return A number on the triangle distribution passed.
 */
T triangular_distribution<T>::operator()(Generator& g, const T& range, const T& mean) {
    T a = super::a(), b = super::b();
    T f_val = (c - a) / (b - a);
    T rand_val = rng(g);
    return ((rand_val < f_val)
            ? a + sqrt(rand_val * (b-a) * (c-a))
            : b - sqrt((1-rand_val) * (b-a) * (b-c))) * range + mean;
}


#pragma once
#include <ctime>
using std::localtime;
using std::time_t;

#include <chrono>
using std::chrono::system_clock;
#include <exception>
using std::exception;
#include <iomanip>
using std::put_time;
#include <iostream>
using std::clog;
using std::cerr;
using std::endl;
#include <string>
using std::string;
#include <mutex>

std::mutex global_log_mutex;

void log(string message) noexcept {
	global_log_mutex.lock();
	system_clock::time_point now = system_clock::now();
	time_t now_c = system_clock::to_time_t(now);
	clog << put_time(localtime(&now_c), "%c %Z") << " [LOG]: "
			<< message << endl;
	global_log_mutex.unlock();
}

void warn(string message) noexcept {
	global_log_mutex.lock();
	system_clock::time_point now = system_clock::now();
	time_t now_c = system_clock::to_time_t(now);
	cerr << put_time(localtime(&now_c), "%c %Z") << " [WARNING]: "
			<< message << endl;
	global_log_mutex.unlock();
}

void error(string message) noexcept {
	system_clock::time_point now = system_clock::now();
	time_t now_c = system_clock::to_time_t(now);
	cerr << put_time(localtime(&now_c), "%c %Z") << " [ERROR]: "
			<< message << endl;
}

[[noreturn]] void error(string message, exception ex) {
	system_clock::time_point now = system_clock::now();
	time_t now_c = system_clock::to_time_t(now);
	cerr << put_time(localtime(&now_c), "%c %Z") << " [ERROR]: "
			<< message << endl;
	throw ex;
}


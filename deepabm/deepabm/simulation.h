#pragma once

#include <fstream>
using std::fstream;
using std::ifstream;
#include <memory>
using std::make_shared;
using std::make_unique;
using std::shared_ptr;
using std::unique_ptr;
#include <sstream>
using std::stringstream;
#include <string>
using std::string;
#include <vector>
using std::vector;

#include "config.h"
#include "core.h"
#include "environment.h"
#include "landowner.h"
#include "farmer.h"
#include "weather_generator.h"

class Simulation : public core<Landowner> {
    enum Command {
        ACT,
        END_EPISODE
    };
    void tick(Command);
    void act(vector<unique_ptr<Landowner>>::iterator,
        vector<unique_ptr<Landowner>>::iterator);
    void end_episode(vector<unique_ptr<Landowner>>::iterator,
        vector<unique_ptr<Landowner>>::iterator);

    enum CSVCol {
        NIL = 0,
    FID = 1,
    LO_TYPE = 2,
    X_COORD = 4,
    Y_COORD = 5
  };

  unique_ptr<vector<shared_ptr<Parcel>>> parcels;

  void read_parcels(ifstream&);

public:
  typedef core super;
  Simulation();
  Simulation(const string&);

  void run_episode(unsigned);
  virtual void tick();
};

[[using std: getline, stoi, stol, stold, stringstream]]
void Simulation::read_parcels(ifstream& csv) {
  string line;
  getline(csv, line); // Remove Header
  
  while (getline(csv, line)) {
    stringstream line_stream(line);
    string cell;
    unsigned index = 0;
    long farmer_id;
    LandownerType landowner_type = LandownerType::NIL;
    vector<long double> params;
    while (getline(line_stream, cell, ',')) {
      switch ((CSVCol) index) {
        case FID:
          farmer_id = stol(cell);
          break;
        case LO_TYPE:
          landowner_type = (LandownerType)stoi(cell);
          break;
        case NIL:
        case X_COORD:
        case Y_COORD:
          break;
        default:
          params.push_back(stold(cell)/config::LAND_SCALE);
          break;
      }
      index++;
    }
    switch (landowner_type) {
      case FARMER:
        parcels->emplace_back(
            make_shared<Parcel>(farmer_id, landowner_type, params));
        agents->emplace_back(
            make_unique<DeepFarmer>(farmer_id, parcels->back()));
        break;
      default:
        /* This is just the farm model. */
        break;
    }
  }
}

#pragma mark - Constructors

Simulation::Simulation(): super() {
  const string& filename = "data/headers.csv";
  parcels = make_unique<vector<shared_ptr<Parcel>>>();
  unique_ptr<ifstream> data = make_unique<ifstream>(filename);
  read_header(*data);
  read_parcels(*data);
}

Simulation::Simulation(const string& filename): super() {
  parcels = make_unique<vector<shared_ptr<Parcel>>>();
  unique_ptr<ifstream> data = make_unique<ifstream>(filename);
  read_header(*data);
  read_parcels(*data);
}

#pragma mark - Actions

void Simulation::act(vector<unique_ptr<Landowner>>::iterator first,
           vector<unique_ptr<Landowner>>::iterator last) {
  for (auto actor = first; actor != last; actor = next(actor))
    (*actor)->act();
}

void Simulation::end_episode(vector<unique_ptr<Landowner>>::iterator first,
            vector<unique_ptr<Landowner>>::iterator last) {
  for (auto actor = first; actor != last; actor = next(actor))
    (*actor)->end_episode();
}

void Simulation::tick(Command command) {
 auto agents_start = agents->begin();
 auto step = (int)(agents->size() / job_count);
 auto agents_stop = agents->end();
 vector<thread> thread_batch(job_count);

 short threadcount = job_count - 1;
 for (int index = 0; index < threadcount; index++) {
  switch (command) {
   case ACT:
    thread_batch[index] = thread(&Simulation::act, this,
      agents_start + (index * step), agents_start + (index+1) * step);
    break;
      case END_EPISODE:
        thread_batch[index] = thread(&Simulation::end_episode, this,
            agents_start + (index * step), agents_start + (index+1) * step);
        break;
  }
 }
  switch (command) {
    case ACT:
      thread_batch[threadcount] = thread(&Simulation::act, this,
                   agents_start + threadcount * step, agents_stop);
      break;
    case END_EPISODE:
      thread_batch[threadcount] = thread(&Simulation::end_episode, this,
                   agents_start + threadcount * step, agents_stop);
      break;
  }
 for (int i = 0; i <= threadcount; i++)
  thread_batch[i].join();
 model_time++;
}

void Simulation::tick() {
  sim_year = 2001;
  tick(ACT);
  tick(END_EPISODE);
}

#pragma mark - Episode Running

void Simulation::run_episode(unsigned episode_length=40) {
  if (verbose_flag)
   cout << "[";
  for (sim_year = 2001; sim_year < 2001+episode_length; sim_year++) {
    // TODO ____________________
    // Generate Weather for Year
    unsigned weather = 0u;
    // Apply Weather to Agents
    Environment cur_year(weather);
    // End
    tick(ACT);
    if (verbose_flag)
     cout << "|";
    if (octave_flag) {
      for (auto actor = agents->begin(); actor != agents->end();
            actor = next(actor)) {
        cout << *(*actor) << ';' << endl;
      }
    }
  }
  tick(END_EPISODE);
  if (verbose_flag)
   cout << "]";
}


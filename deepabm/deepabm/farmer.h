//
//  farmer.h
//  deepabm
//
//  Created by Kevin Andrew on 6/15/18.
//  Copyright © 2018 Kevin Andrew. All rights reserved.
//

#pragma once

#include <algorithm>
using std::max_element;
using std::min;
#include <cassert>
#include <cmath>
using std::exp;
using std::pow;
#include <deque>
using std::deque;
#include <memory>
using std::make_shared;
using std::make_unique;
using std::shared_ptr;
using std::unique_ptr;
#include <mutex>
#include <random>
using std::default_random_engine;
using std::uniform_int_distribution;
#include <set>
using std::set;
#include <utility>
using std::pair;

#include "config.h"
using config::CORN_PROD;
using config::HAY_PROD;
using config::MEAT_PROD;
using config::MILK_PROD;
using config::HAY_PHOS;
using config::CORN_PHOS;
using config::COW_PHOS;
#include "dbmexcept.h"
#include "environment.h"
#include "learning.h"
#include "landowner.h"
#include "neural_net.h"
#include "triangular_distribution.h"

class DeepFarmer : public Landowner {
    // Random Number Generators (some may be externalized later)
    static default_random_engine generator;
    static triangular_distribution<long double> basic_triangle;
    static triangular_distribution<long double> hay_factor_generator;
    static triangular_distribution<long double> corn_factor_generator;
    static triangular_distribution<long double> meat_factor_generator;
    static triangular_distribution<long double> milk_factor_generator;
    static triangular_distribution<long double> hay_phos_generator;
    static triangular_distribution<long double> corn_phos_generator;
    static triangular_distribution<long double> cow_phos_generator;
    
protected:
    unsigned long max_replay = 10e4; // No of State Transitions in Memory
    
    /* No of Samples Taken for Policy Determination
       Higher = Smoother Gradients = More Accurate Convergence = Slower */
    unsigned long memory_max = 16;

    // Simple Policies for Testing
    enum Policy {
        GREEDY,
        NONGREEDY,
        RANDOM
    } policy = NONGREEDY;
    
    // Farmer Outputs
    enum Product {
        HAY,
        CORN,
        MEAT,
        MILK,
        PHOS
    };
   
private:
    uniform_int_distribution<unsigned long> memory_generator =
            uniform_int_distribution<unsigned long>(0, max_replay);
    
    /**
     The state of the farm at time t.
     */
    struct FarmState: State {
        static const unsigned size = 13; // No of Parameters
        struct {
            long double hay = 0.0L;
            long double corn = 0.0L;
            long double meat = 0.0L;
            long double milk = 0.0L;
            long double phos = 0.0L;
        } harvest, original_harvest;
        struct {
            long double tech = 0.0L;
        } bmp;
        struct {
            long double hay;
            long double corn;
            long double meat;
            long double milk;
            struct {
                long double hay;
                long double corn;
                long double cow;
            } phos;
        } production_factor, original_factor;
        FarmState(DeepFarmer* owner = nullptr) : State(owner) {}
        FarmState(const FarmState& other): State(other.land_owner) {
            State::init();
            harvest = other.harvest;
            bmp = other.bmp;
            production_factor = other.production_factor;
            original_factor = other.original_factor;
        }
        void init() {
            State::init();
            original_factor = production_factor;
        }
        /**
         Resets the farm's state to its original condition.
         */
        void reset() noexcept {
            production_factor = original_factor;
            harvest = original_harvest;
            bmp.tech = 0.0L;
        }
        operator vector<long double>() {
            vector<long double> to_vec = land_areas;
            to_vec.push_back(production_factor.hay);
            to_vec.push_back(production_factor.corn);
            to_vec.push_back(production_factor.meat);
            to_vec.push_back(production_factor.milk);
            return to_vec;
        }
        [[nodiscard]] long double get() noexcept {
            return harvest.phos;
        }
    } current_state;
    
    /**
     Actions a Farmer can take.
     */
    struct FarmAction: Action {
        static const unsigned size = 5;
        // Production Factors
        struct {
            long double hay;
            long double corn;
            long double meat;
            long double milk;
        } producing;
        
        // Simple BMP Adoption
        long double tech_choice;
        
        FarmAction() {}
        FarmAction(vector<long double> acts) {
            producing.hay = acts[0];
            producing.corn = acts[1];
            producing.meat = acts[2];
            producing.milk = acts[3];
            tech_choice = acts[4];
        }
        FarmAction(const FarmAction& other) {
            producing = other.producing;
            tech_choice = other.tech_choice;
        }
        [[nodiscard]] long double operator[] (unsigned index) {
            switch (index) {
                case 0: return producing.hay;
                case 1: return producing.corn;
                case 2: return producing.meat;
                case 3: return producing.milk;
                case 4: return tech_choice;
                default:
                    throw agent_exception("Attempting Unknown Action: " 
                        + to_string(index));
            }
        }
        operator vector<long double>() {
            return {{producing.hay, producing.corn, producing.meat,
                    producing.milk, tech_choice}};
        }
    } selected_action;
    
    void init();
    void remember(Record);
    [[nodiscard]] long double bound_action(long double, const long double&,
        const long double&, const long double&) noexcept;
    [[nodiscard]] vector<Record> remember();
    [[nodiscard]] pair<long double, long double> produce(Product);
    [[nodiscard]] Record execute();

protected:
    unique_ptr<neural_net> actor;
    unique_ptr<neural_net> critic;
    unique_ptr<neural_net> target_actor;
    unique_ptr<neural_net> target_critic;
    learning_rate alpha_u = 0.01; // 0.01;
    learning_rate alpha_q = 0.05; // 0.05;
    discount_rate gamma = 0.9;
    transfer_rate tau = 0.001;
    
    deque<Record> replay;
    
    [[nodiscard]] vector<long double> get_policy_gradient(Record,
        vector<Record>);

    string to_ostr() {
      return to_string(id) + " "
          + to_string(current_state.production_factor.hay)
          + " " + to_string(current_state.bmp.tech);
    }
    
public:
    typedef Landowner super;
    DeepFarmer(unsigned);
    DeepFarmer(unsigned, shared_ptr<Parcel>);
    
    void act(shared_ptr<Environment>);
    void end_episode();
    void reset();
};

#pragma mark - Statics

default_random_engine DeepFarmer::generator =
    default_random_engine((*RANDOM_DEVICE)());

triangular_distribution<long double> DeepFarmer::basic_triangle = 
    triangular_distribution<long double>();
triangular_distribution<long double>
    DeepFarmer::hay_factor_generator(HAY_PROD.MIN, HAY_PROD.MAX);
triangular_distribution<long double>
    DeepFarmer::corn_factor_generator(CORN_PROD.MIN, CORN_PROD.MAX);
triangular_distribution<long double>
    DeepFarmer::meat_factor_generator(MEAT_PROD.MIN, MEAT_PROD.MAX);
triangular_distribution<long double>
    DeepFarmer::milk_factor_generator(MILK_PROD.MIN, MILK_PROD.MAX);
triangular_distribution<long double>
    DeepFarmer::hay_phos_generator(HAY_PHOS.MIN, HAY_PHOS.MAX);
triangular_distribution<long double>
    DeepFarmer::corn_phos_generator(CORN_PHOS.MIN, CORN_PHOS.MAX);
triangular_distribution<long double>
    DeepFarmer::cow_phos_generator(COW_PHOS.MIN, COW_PHOS.MAX);

#pragma mark - Construction

/**
 Initializes Farmer Parameters and Nerual Nets
 */
void DeepFarmer::init() {
    current_state = FarmState(this);
    current_state.production_factor.hay = hay_factor_generator(generator);
    current_state.production_factor.corn = corn_factor_generator(generator);

    long double cownt = 3.0L / config::COW_SCALE;
    current_state.production_factor.meat = cownt
        * meat_factor_generator(generator)
        / basic_triangle(generator, 2, 3);
    current_state.production_factor.milk = cownt
        * milk_factor_generator(generator);
    
    current_state.production_factor.phos.hay =
        hay_phos_generator(generator);
    current_state.production_factor.phos.corn =
        corn_phos_generator(generator);
    current_state.production_factor.phos.cow = cownt 
        * cow_phos_generator(generator);
    
    actor = make_unique<neural_net>(
        FarmState::size, 5, 15, FarmAction::size, alpha_u);
    critic = make_unique<neural_net>(
        FarmState::size + FarmAction::size, 5, 15, 1, alpha_q);
    target_actor = make_unique<neural_net>(*actor);
    target_critic = make_unique<neural_net>(*critic);

    if (policy_val != -1)
      switch (policy_val) {
        case 1:
          policy = GREEDY;
          break;
        case 2:
          policy = NONGREEDY;
          break;
        case 3:
          policy = RANDOM;
          break;
      }
}

DeepFarmer::DeepFarmer(unsigned id): super(id) {
    init();
}

DeepFarmer::DeepFarmer(unsigned id, shared_ptr<Parcel> parcel):
        super(id, parcel) {
    init();
    current_state.init();
}

#pragma mark - Accessories

/**
 Gets the policy gradient for the current S-A-R-S tuple compared to others 
 in memory.
 [Policy Gradient RL for Fast Quadrapedal Locomotion, Kohl and Stone, 2004]

 @param current The most recent state transition Record.
 @param records A selection of state transion Records from memory.
 @return The policy gradient for taking action A compared to other actions.
 */
vector<long double> DeepFarmer::get_policy_gradient(Record current,
        vector<Record> records) {
    vector<long double> gradient(FarmAction::size);

    for (unsigned factor = 0; factor < FarmAction::size; factor++) {
        struct {
            long count = 0;
            long double total = 0.0L;
            inline long double mean() {
                return (total == 0.0L) ? 0.0L : count / total;
            };
        } pos, neg, eq;
        
        for (Record record: records) {
            auto record_factor = (*record.action)[factor];
            auto current_factor = (*current.action)[factor];
            bool gradIsPos = record_factor > current_factor;
            bool gradIsNeg = record_factor < current_factor;
            if (gradIsPos and (pos.count = pos.count + 1))
                pos.total += record.reward;
            else if (gradIsNeg and (neg.count = neg.count + 1))
                neg.total += record.reward;
            else if ((eq.count = eq.count + 1))
                eq.total += record.reward;
            else
                throw learning_exception("Problem Calculating Gradient");
        }
        
        long double eqq = eq.mean(), posq = pos.mean(), negq = neg.mean();
        gradient[factor] = ((eqq > posq) && (eqq > negq)) ? 0.0L 
            : (posq - negq);
    }
    long double max_gradient = *max_element(gradient.begin(),
        gradient.end());
    long double eta = 0.05L;
    for (unsigned factor = 0; factor < FarmAction::size; factor++) {
        gradient[factor] /= (max_gradient == 0.0L) ? 1.0L : max_gradient;
        gradient[factor] *= eta;
    }
    return gradient;
}

#pragma mark - Background Action

/**
 Returns production function.

 @param product Type of product being produced on farm.
 @return Pair of <amount produced, value of production>.
 */
pair<long double, long double> DeepFarmer::produce(Product product) {
    pair<long double, long double> returns;
    [[maybe_unused]] long double area;
    long double prod = 0.0L, profit = 0.0L;
    switch (product) {
        case HAY:
            if (current_state.production_factor.hay < HAY_PROD.MIN) {
                profit = (9.0L * current_state.production_factor.hay) 
                    - 10.0L;
            } else {
                area = owned_parcel_area(Parcel::PASTURE)
                    * config::LAND_SCALE;
                prod = current_state.production_factor.hay 
                    * pow(area, current_state.bmp.tech);
                profit = prod * 1e-32 * exp(sim_year * 0.0358);
            }
            break;
        case CORN:
            if (current_state.production_factor.corn < CORN_PROD.MIN) {
                profit = (9.0L * current_state.production_factor.corn)
                    - 10.0L;
            } else {
                area = owned_parcel_area(Parcel::CROP) * config::LAND_SCALE;
                prod = current_state.production_factor.corn
                    * pow(area, 1.0L * current_state.bmp.tech);
                profit = prod * (11.433 * log(sim_year) - 86.826);
            }
            break;
        case MEAT:
            if (current_state.production_factor.meat < MEAT_PROD.MIN) {
                profit = (9.0L * current_state.production_factor.meat)
                    - 10.0L;
            } else {
                area = owned_parcel_area(Parcel::PASTURE)
                    * config::LAND_SCALE;
                prod = current_state.production_factor.meat
                    * pow(area, current_state.bmp.tech);
                profit = prod * 2e-20 * exp(sim_year * 0.0234L);
            }
            break;
        case MILK:
            if (current_state.production_factor.milk < MILK_PROD.MIN) {
                profit = (9.0L * current_state.production_factor.milk)
                    - 10.0L;
            } else {
                area = owned_parcel_area(Parcel::PASTURE)
                    * config::LAND_SCALE;
                prod = current_state.production_factor.milk
                    * pow(area, current_state.bmp.tech);
                profit = 2e-9 * exp(sim_year * 0.0114L);
            }
            break;
        case PHOS:
            {
                long double pasture = owned_parcel_area(Parcel::PASTURE)
                    * config::LAND_SCALE;
                long double crop = owned_parcel_area(Parcel::CROP)
                    * config::LAND_SCALE;
                prod = current_state.production_factor.phos.hay
                    * pow(pasture, current_state.bmp.tech);
                prod += current_state.production_factor.phos.cow 
                    * pow(pasture, current_state.bmp.tech);
                prod += current_state.production_factor.phos.corn 
                    * pow(crop, current_state.bmp.tech);
            }
            break;
        default:
            throw agent_exception("Unable to produce type: " 
                + to_string(product));
    }
    returns.first = prod;
    returns.second = profit;
    return returns;
}

/**
 Bounds a value to a range with given mean and limits.

 @param value The value to be limited.
 @param mean_value The mean value of the resulting distribution.
 @param max_value The maximum bound of conversion.
 @param min_value The minimum bound of conversion. Default 0.0L.
 @return The value bounded to the provided range.
 */
long double DeepFarmer::bound_action(long double value,
        const long double& mean_value, const long double& max_value,
        const long double& min_value=0.0L) noexcept {
    long double bound_value = value + mean_value;
    if (bound_value > max_value)
        return max_value;
    else if (bound_value < min_value)
        return min_value;
    else
        return bound_value;
}

/**
 Excute the current selected action and apply the results to the current 
 state.

 @return The S-A-R-S Record from taking the selected action from the 
    current state.
 */
[[nodiscard]] DeepFarmer::Record DeepFarmer::execute() {
    Record current_record(new FarmState(current_state),
        new FarmAction(selected_action));
    
    // Take the Action
    current_state.production_factor.hay =
        bound_action(selected_action.producing.hay, HAY_PROD.MEAN,
        HAY_PROD.MAX, HAY_PROD.MIN/20.0);
    current_state.production_factor.corn =
        bound_action(selected_action.producing.corn, CORN_PROD.MEAN,
        CORN_PROD.MAX, CORN_PROD.MIN/20.0);
    current_state.production_factor.meat =
        bound_action(selected_action.producing.meat, MEAT_PROD.MEAN,
        MEAT_PROD.MAX, MEAT_PROD.MIN/20.0);
    current_state.production_factor.milk =
        bound_action(selected_action.producing.milk, MILK_PROD.MEAN,
        MILK_PROD.MAX, MILK_PROD.MIN/20.0);
    current_state.bmp.tech = (selected_action.tech_choice < 0.0L) ? 1.1L 
        : 0.5L;
    
    current_state.init();
    
    // Record Results
    pair<long double, long double> returns_hay = produce(HAY);
    pair<long double, long double> returns_corn = produce(CORN);
    pair<long double, long double> returns_meat = produce(MEAT);
    pair<long double, long double> returns_milk = produce(MILK);
    pair<long double, long double> returns_phos = produce(PHOS);
    current_state.harvest.hay = returns_hay.first;
    current_state.harvest.corn = returns_corn.first;
    current_state.harvest.meat = returns_meat.first;
    current_state.harvest.milk = returns_milk.first;
    current_state.harvest.phos = returns_phos.first;
    switch (policy) {
        case GREEDY:
            current_record.reward = returns_hay.second
                + returns_corn.second + returns_meat.second
                + returns_milk.second;
            break;
        case NONGREEDY:
            {
                bool[[likley]] produces_phos = returns_phos.first > 0;
                if (produces_phos)
                    current_record.reward = 40 - returns_phos.first;
                else
                    current_record.reward = 40;
            }
            break;
        case RANDOM:
            current_record.reward = basic_triangle(generator, 1, 0.0L);
            break;
        default:
            throw learning_exception("Unknown Policy");
    }
    current_record.next_state = new FarmState(current_state);
    return current_record;
}

/**
 Stores a record in agent memory. If there is no more room,
 the oldest memory is removed.

 @param record State transition Record to be added to memory.
 */
void DeepFarmer::remember(Record record) {
    if (replay.size() >= max_replay)
        replay.pop_front();
    replay.emplace_back(record);
}

/**
 Retrives a selection of at most memory_max memories from agent memory.

 @return A random selection of state transition Records from memory.
 */
vector<DeepFarmer::Record> DeepFarmer::remember() {
    vector<Record> memories;
    set<unsigned long> indices;
    auto memory_limit = min((long unsigned) replay.size(), memory_max);
    do {
        unsigned long index = memory_generator(generator) % replay.size();
        if (indices.insert(index).second)
            memories.push_back(replay[index]);
    } while (memories.size() < memory_limit);
    return memories;
}

#pragma mark - Acting

/**
 Perform a yearly action with DDQN learning.
 
 [Deep Reinforcement Learning with Double Q-learning,
    Hasselt Guez and Silver, 2015]
 */
void DeepFarmer::act(shared_ptr<Environment> environment=nullptr) {
    if (environment and environment->storm_data()) {
      // TODO: use generated storm events for loss calculation
    }
    selected_action = FarmAction(actor->feed(current_state)->get_output());
    Record current_record = execute();
    remember(current_record);
    
    vector<Record> memories = remember();
    long double loss = 0.0L;
    for (Record memory : memories) {
        auto target_action =
            target_actor->feed(*memory.next_state)->get_output();
        auto target_ranking = target_critic->feed(*memory.next_state,
            target_action)->get_out();
        auto expected_target = critic->feed(*memory.state,
            *memory.action)->get_out();
        long double target = memory.reward + gamma * target_ranking;
        loss += (target - expected_target)
            / min((long unsigned int)replay.size(), memory_max);
    }
    auto J = get_policy_gradient(current_record, memories);
    actor->feed_grad(J);
    critic->feed(loss);
}

void DeepFarmer::end_episode() {
    target_actor->transfer_in(*actor, tau);
    target_critic->transfer_in(*critic, tau);
    current_state.reset();
}

void DeepFarmer::reset() {
    current_state.reset();
}


#pragma once

#include <vector>

class weather_generator {
  // generate a random number of extreme storms
  // and: get that number
  virtual unsigned storm_count() = 0;
  // and: get the storm severities
  virtual std::vector<unsigned> storms() = 0;
};


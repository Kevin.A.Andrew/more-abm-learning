CC=g++
CFLAGS=-std=c++2a -Ofast
DFLAGS=-std=c++2a -g -Og
OFLAGS=-std=c++1z -Ofast
WFLAGS=-Wall -Wextra -Wpedantic -Wno-unknown-pragmas -Wno-attributes
LIBS=-lpthread
TGT=dabm
SRC=deepabm/deepabm
DOCS=docs

all:
	$(CC) $(SRC)/main.cpp $(SRC)/*.cxx -o $(TGT) $(CFLAGS) $(WFLAGS) $(LIBS)

debug:
	$(CC) $(SRC)/main.cpp $(SRC)/dbmexcept.cxx -o $(TGT) $(DFLAGS) $(WFLAGS) $(LIBS)

c17:
	$(CC) $(SRC)/main.cpp $(SRC)/dbmexcept.cxx -o $(TGT) $(OFLAGS) $(WFLAGS) $(LIBS)

deprecated:
	@echo
	@echo [WARN] Target has been deprecated. See makefile for more info.
	@echo

old: deprecated
	$(CC) $(SRC)/main.cpp -o $(TGT) $(OFLAGS) $(WFLAGS) $(LIBS)

clean:
	-@rm $(TGT) $(TGT).exe 2>/dev/null ||:

.PHONY: all clean debug deprecated old


echo "Greedy Run"
./dabm -oDATG -j 7 -p GREEDY -e 250 > glog.m
echo "Non-Greedy Run"
./dabm -oDATN -j 7 -p NONGREEDY -e 250 > nglog.m
echo "Random Run"
./dabm -oDATR -j 7 -p RANDOM -e 250 > rlog.m


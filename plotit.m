function plotit(DAT, COL)
  pkg load statistics;
  DATA_SZ = size(DAT);
  % DATA = reshape(DAT, DATA_SZ(1)/(480*40), 40, 480, 3);
  IDS = unique(DAT(:,1));
  SORTED_RUNS = zeros(480, DATA_SZ(1)/480);
  IDX = 1;
  for ID = IDS'
    SORTED_RUNS(IDX, :) = DAT(DAT(:) == ID, :)(:,COL)';
    IDX = IDX + 1;
  endfor
  plot(1:(DATA_SZ(1)/480), nanmean(SORTED_RUNS));
endfunction
